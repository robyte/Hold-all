// pages/weather/weather.js
//获取应用实例
const app = getApp();
const day = ["今天", "明天", "后天"];
let interstitialAd = null;
Page({
    data: {
        day: day
    },

    onShow: function () {
        const that = this;
        wx.getSetting({
            success: function (res) {
                const auth = res.authSetting['scope.userLocation'];
                console.log(auth);
                if (!auth) {
                    wx.hideLoading();
                    wx.showModal({
                        title: '警告',
                        confirmText: '授权',
                        content: '小程序未获取到当前位置授权信息，无法展示天气数据，请前往设置页面进行授权',
                        showCancel: false,
                        success: function (res) {
                            wx.openSetting({})
                        }
                    })
                } else if (auth) {
                    wx.showLoading({
                        title: '加载中...',
                        mask: true
                    })
                    that.getLocation();
                    setTimeout(() => {
                        if (interstitialAd) {
                            interstitialAd.show().catch((err) => {
                                console.error(err)
                            })
                        }
                    }, 1000)
                }
            },
            fail: function (res) {
                console.log(res);
            }
        })
    },

    onLoad: function () {
        const that = this;
        that.getLocation();
        wx.showLoading({
            title: '加载中...',
            mask: true
        })

        if (wx.createInterstitialAd) {
            interstitialAd = wx.createInterstitialAd({adUnitId: 'adunit-07988ac4bf8362c2'})
        }
    },

    //获取经纬度方法
    getLocation: function () {
        let temp = 0;
        const that = this;
        wx.getLocation({
            type: 'wgs84',
            success: function (res) {
                temp = 0;
                console.log(res);
                const latitude = res.latitude;
                const longitude = res.longitude;
                that.getCity(latitude, longitude);
            },
            fail: function (e) {
                if (temp > 3) {
                    wx.hideLoading();
                    wx.showModal({
                        title: '警告',
                        content: '小程序未获取到当前位置授权信息，无法展示天气数据，请前往设置页面进行授权',
                        showCancel: false,
                        success: function (res) {
                            wx.openSetting({})
                        }
                    })
                } else {
                    temp++;
                    that.getLocation();
                }
            }
        })
    },

    //获取城市信息
    getCity: function (latitude, longitude) {
        const that = this;
        const url = "https://api.map.baidu.com/geocoder/v2/";
        const params = {
            ak: "Dk12tC0IkL653G6BCL5VGBSO1uylG9Ys",
            output: "json",
            location: latitude + "," + longitude
        };
        wx.request({
            url: url,
            data: params,
            success: function (res) {
                console.log(res);
                const city = res.data.result.addressComponent.city;
                const district = res.data.result.addressComponent.district;
                const street = res.data.result.addressComponent.street;

                that.setData({
                    city: city,
                    district: district,
                    street: street,
                })
                that.getWeahter(city);
            },
            fail: function (res) {
            },
            complete: function (res) {
            },
        })
    },

    //获取天气信息
    getWeahter: function (city) {
        console.log("定位城市为：" + city);
        const that = this;
        const url = "https://free-api.heweather.net/s6/weather";
        const params = {
            location: city,
            key: "bdd18de10e7148c2bdef1f1b2d514ef7"
        };
        wx.request({
            url: url,
            data: params,
            success: function (res) {
                console.log(res.data.HeWeather6[0]);
                const tmp = res.data.HeWeather6[0].now.tmp;
                const txt = res.data.HeWeather6[0].now.cond_txt;
                const code = res.data.HeWeather6[0].now.cond_code;
                const qlty = res.data.HeWeather6[0].lifestyle[7].brf;
                const dir = res.data.HeWeather6[0].now.wind_dir;
                const sc = res.data.HeWeather6[0].now.wind_sc;
                const hum = res.data.HeWeather6[0].now.hum;
                const fl = res.data.HeWeather6[0].now.fl;
                const daily_forecast = res.data.HeWeather6[0].daily_forecast;
                that.setData({
                    tmp: tmp,
                    txt: txt,
                    code: code,
                    qlty: qlty,
                    dir: dir,
                    sc: sc,
                    hum: hum,
                    fl: fl,
                    daily_forecast: daily_forecast
                })
                wx.hideLoading()
            },
            fail: function (res) {

            },
            complete: function (res) {

            }
        })
    }
})