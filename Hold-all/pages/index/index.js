//index.js
//获取应用实例
const app = getApp()
let interstitialAd = null;
Page({
    data: {
        msg: '更多功能加速开发中...',
        itemSet: [
            {
                name: '天气',
                img: '../../image/weather.png',
                representative: 'weather'
            },
            {
                name: '计算器',
                img: '../../image/calculator.png',
                representative: 'calculator'
            },
            // {
            //   name: '货币汇率',
            //   img: '',
            //   representative: 'currency'
            // },
            {
                name: '查拼音',
                img: '../../image/pinyin.png',
                representative: 'pinyin'
            },
            {
                name: '二维码生成',
                img: '../../image/qrcode.png',
                representative: 'qrcode'
            },
            {
                name: '时钟',
                img: '../../image/clock.png',
                representative: 'clock'
            },
            // {
            //   name: '录音',
            //   img: '',
            //   representative: 'record'
            // },
            {
                name: '幸运骰子',
                img: '../../image/dice.png',
                representative: 'dice'
            },
            {
                name: '涂鸦',
                img: '../../image/doodling.png',
                representative: 'doodling'
            }
            // {
            //   name: '五险一金计算',
            //   img: '',
            //   representative: 'money'
            // }
        ]
    },
    //事件处理函数
    bindViewTap: function (event) {
        const data = event.currentTarget.dataset;
        const item = this.data.itemSet;
        console.log(item[0]);
        wx.navigateTo({
            url: '../' + item[data.index].representative + '/' + item[data.index].representative
        })
    },

    onLoad: function () {
        if (wx.createInterstitialAd) {
            interstitialAd = wx.createInterstitialAd({adUnitId: 'adunit-07988ac4bf8362c2'})
            interstitialAd.onLoad(() => {
                console.log('onLoad event emit')
            })
            interstitialAd.onError((err) => {
                console.log('onError event emit', err)
            })
            interstitialAd.onClose((res) => {
                console.log('onClose event emit', res)
            })
        }
        setTimeout(() => {
            if (interstitialAd) {
                interstitialAd.show().catch((err) => {
                    console.error(err)
                })
            }
        }, 1000)
    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        return {
            title: '各种实用小工具',
            path: 'pages/index/index'
        }
    }
})
